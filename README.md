# Loterias API

Protótipo de API baseada em Flask, para consultar resultados da Quina e da Mega-Sena por número ou data de concurso, exibindo resultados no formato JSON.

## Lista de tarefas

- [x] Iniciar API com resultados da Mega-Sena
- [x] Implementar resultados da Quina
- [x] Acrescentar páginas com detalhes estatísticos (Pandas Profiling)
- [ ] Implementar consultas por grupos de dezenas (apostas)
- [ ] Refatorar usando FastAPI


## Instalação

Num ambiente virtual Python (*virtualenv*), use o gerenciador de pacotes [pip](https://pip.pypa.io/en/stable/) para instalar as dependências do arquivo requirements.txt.

```bash
pip install -r requirements.txt
```

## Uso

No terminal, execute o comando:

```bash
python loteriasAPI.py
```
Depois, basta acessar o endereço:porta 127.0.0.1:5000 no seu navegador e fazer as consultas.

## Contribuindo
Código de estudos pessoais.

## Licença
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
