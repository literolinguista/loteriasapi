import flask, sqlite3
from flask import Flask, render_template, request, jsonify

app = flask.Flask(__name__)  
# app.config["DEBUG"] = True

def gerar_dicionario(cursor, row):
    dicionario = {}
    for idx, col in enumerate(cursor.description):
        dicionario[col[0]] = row[idx]
    return dicionario


@app.route('/')
def home():
    return render_template('home.html')

@app.route('/api/v1/ver/quina/todos')
def ver_todos_quina():
    conn = sqlite3.connect('dados/quinaResultados.db')
    conn.row_factory = gerar_dicionario
    cur = conn.cursor()
    todos = cur.execute('SELECT * FROM quinaResultados;').fetchall()
    return jsonify(todos)

@app.route('/api/v1/ver/quina')
def ver_filtro_quina():
    itens_pesquisa = request.args

    concurso = itens_pesquisa.get('concurso')
    data = itens_pesquisa.get('data')

    pesquisa = "SELECT * FROM quinaResultados WHERE"
    filtrar = []

    if concurso:
        pesquisa += ' concurso=? AND'
        filtrar.append(concurso)
    if data:
        pesquisa += ' data=? AND'
        filtrar.append(data)
    if not (concurso or data):
        return pagina_nao_encontrada(404)

    pesquisa = pesquisa[:-4] + ';'

    conn = sqlite3.connect('dados/quinaResultados.db')
    conn.row_factory = gerar_dicionario
    cur = conn.cursor()

    resultados = cur.execute(pesquisa, filtrar).fetchall()
    return jsonify(resultados)


@app.route('/api/v1/ver/sena/todos')
def ver_todos_sena():
    conn = sqlite3.connect('dados/megaSenaResultados.db')
    conn.row_factory = gerar_dicionario
    cur = conn.cursor()
    todos = cur.execute('SELECT * FROM megaSenaResultados;').fetchall()
    return jsonify(todos)

@app.route('/api/v1/ver/sena')
def ver_filtro_sena():
    itens_pesquisa = request.args

    concurso = itens_pesquisa.get('concurso')
    data = itens_pesquisa.get('data')

    pesquisa = "SELECT * FROM megaSenaResultados WHERE"
    filtrar = []

    if concurso:
        pesquisa += ' concurso=? AND'
        filtrar.append(concurso)
    if data:
        pesquisa += ' data=? AND'
        filtrar.append(data)
    if not (concurso or data):
        return pagina_nao_encontrada(404)

    pesquisa = pesquisa[:-4] + ';'

    conn = sqlite3.connect('dados/megaSenaResultados.db')
    conn.row_factory = gerar_dicionario
    cur = conn.cursor()

    resultados = cur.execute(pesquisa, filtrar).fetchall()
    return jsonify(resultados)

@app.route('/sobre')
def sobre():
    return render_template('sobre.html')

@app.route('/estatisticas')
def estatisticas():
    return render_template('estatisticas.html')

@app.route('/quina')
def quina():
    return render_template('quina.html')

@app.route('/mega-sena')
def megasena():
    return render_template('mega-sena.html')


@app.errorhandler(404)
def pagina_nao_encontrada(e):
    return render_template('404.html')

if __name__ == '__main__':
    app.run(debug=True)
